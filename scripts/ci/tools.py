import os

import fileutil


def get_tool_path(tool):
    tools_path = fileutil.resove_file_path('../../.tmp')
    return os.path.join(tools_path, tool)


def opm():
    return get_tool_path("opm")


def yq():
    return get_tool_path("yq")


def kustomize():
    return get_tool_path("kustomize")


def controller_gen():
    return get_tool_path("controller-gen")


def operator_sdk():
    return get_tool_path("operator-sdk")
