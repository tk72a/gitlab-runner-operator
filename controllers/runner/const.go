package runner

const (
	// GitLabRunnerServiceAccount is the name of the service account
	// used by the GitLab Runner pod
	GitLabRunnerServiceAccount string = "gitlab-runner-sa"

	// GitLabRunnerRole is the name of the role that defines
	// permissions for the GitLab Runner service account
	GitLabRunnerRole string = "gitlab-runner-app-role"

	// GitLabRunnerRoleBinding resource assigns the role to the
	// GitLab Runner service account
	GitLabRunnerRoleBinding string = "gitlab-runner-app-rolebinding"
)

// CustomError type for defining error constants
type CustomError string

func (e CustomError) Error() string {
	return string(e)
}
